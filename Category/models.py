from django.db import models


# Create your models here.
class Category(models.Model):
    id_category = models.AutoField(primary_key=True, unique=True)
    Category_name = models.CharField(max_length=250)

    def __str__(self):
        return self.Category_name

