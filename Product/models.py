from django.contrib.auth.models import User
from django.db import models


# Create your models here.



class Product(models.Model):
    id_product = models.AutoField(primary_key=True, unique=True, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.FloatField()
    stock = models.IntegerField()
    iva_product = models.FloatField()
    image = models.ImageField(upload_to='product/')
    delete = models.BooleanField(default=False)
    product_list = models.ForeignKey(User,on_delete=models.CASCADE, null=True, blank=True)
    
    def __str__(self):
        return self.name


