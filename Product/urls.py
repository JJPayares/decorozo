from django.urls import path
from Product import views
from Product.views import ProductDelete

urlpatterns = [

    path('product', views.product_register, name='producto'),
    path('catalog/', views.ProductView.as_view(), name='catalogo'),
    path('delete_product/<int:pk>', ProductDelete.as_view(), name='todelete')
]