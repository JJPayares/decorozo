# Generated by Django 4.0.3 on 2022-04-06 03:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0005_alter_product_id_product'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='id_product',
            field=models.AutoField(editable=False, primary_key=True, serialize=False, unique=True),
        ),
    ]
