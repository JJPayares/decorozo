# Generated by Django 4.0.3 on 2022-04-07 11:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0011_alter_imageproduct_table_alter_product_table'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='product',
            table='products',
        ),
    ]
