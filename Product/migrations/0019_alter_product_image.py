# Generated by Django 4.0.3 on 2022-04-07 15:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0018_product_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(upload_to='product/'),
        ),
    ]
