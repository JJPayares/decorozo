from pyexpat import model

from django.contrib.auth.forms import UserCreationForm
from django import forms


from Product.models import Product


class ProductCreation(forms.ModelForm):
    """
    Formulario de creacion de productos, basado en el modelo de productos. Se le asigna a cada compo un widget,
    y a este se le asignan los estilos CSS que recibirá el elemento html renderizado.
    """

    class Meta:
        model = Product
        fields = ['name', 'description', 'price', 'stock', 'iva_product', 'image']

    name = forms.CharField(max_length=50, label='Nombre del producto',
                           widget=forms.TextInput(attrs={'class': 'form-control form-login','placeholder': 'Escriba el nombre del producto...'}), required=True)

    description = forms.CharField(max_length=610, min_length=20, label='Descripcion',
                                  widget=forms.Textarea(attrs={'class': 'form-control form-login','placeholder': 'Añada una breve descripcion...', }),
                                  required=True)

    price = forms.FloatField(max_value=999999, min_value=0, label='Precio', required=True,
                             widget=forms.NumberInput(attrs={'class': 'form-control form-login','placeholder': 'Precio del producto...'}))
    stock = forms.IntegerField(max_value=999, min_value=1, label='Inventario', required=True,
                               widget=forms.NumberInput(attrs={'class': 'form-control form-login','placeholder': 'Cantidad en stock...'}))
    """image = models.ImageField(upload_to='')"""

    iva_product = forms.ChoiceField(choices=[(0, 0.0),(1, 0.05), (2, 0.19)], required=True,
                                    widget=forms.Select(attrs={'class': 'form-control form-login','placeholder': 'IVA del producto...'}))
    image = forms.ImageField(required=True, widget=forms.FileInput(attrs={'class': 'form-control btn btn-danger','placeholder': 'Imagen del producto...'}))
    



class ProductDeleteForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['delete']
        delete = forms.BooleanField()
