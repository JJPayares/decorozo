from django.contrib import messages
from django.shortcuts import render, redirect
# Create your views here.
from django.views.generic import ListView, UpdateView
from Product.forms import ProductCreation, ProductDeleteForm
from Product.models import Product

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin


@login_required(login_url='login')
def product_register(request):
    """
    Vista utilizada para generar un formulario html que recibe los datos requeridos para almacenar en DB un registro
    correpondiente a un producto. En base al tipo de REQUEST que recibe, se puede solamente cargar el formulario
    siendo una peticion GET, o si es POST captura los datos enviados, y segun las validaciones almacenar o no el registro
    en DB.
    :param request:
    :return:
    """
    if request.method == "POST":
        form = ProductCreation(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success = (request, 'Producto registrado')
            return redirect('/catalog/')
        messages.error(request, "Olvidaste un campo, tranquilo prueba de nuevo")
    context = {"form": ProductCreation()}
    return render(request, 'products/register_products.html', context)


class ProductView(ListView):
    """
    Vista encargada de enviar por contexto a la template indicada los registros almacenados en la tabla Product de la
    DB.
    """
    model = Product
    context_object_name = 'product'
    template_name = 'products/product_list.html'


class ProductDelete(LoginRequiredMixin, UpdateView):
    """
    Vista encargada de cambiar el estado de visibilidad en DB del producto cuya PK se le indique.
    DB.
    """
    model = Product
    form_class = ProductDeleteForm
    pk_url_kwarg = 'pk'
    template_name = 'products/delete_product.html'
    success_url = '/catalog/'
    login_url = 'login'
