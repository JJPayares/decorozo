from django.db import models
from django.contrib.auth.models import User
from Product.models import Product


# Create your models here.
class ToDispatch(models.Model):
    id_dispatch = models.CharField(max_length=50, primary_key=True, unique=True, null=False)
    id_user = models.ForeignKey(User, on_delete=models.RESTRICT)
    id_product = models.ForeignKey(Product, on_delete=models.RESTRICT)
    quantity = models.IntegerField()
    date = models.DateField(auto_now=True)
    sell_state = models.BooleanField(default=True)

    def __str__(self):
        return f'Dispacth #{self.id_dispatch}'
