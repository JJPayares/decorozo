from django.shortcuts import render, redirect

from .forms import NewUserForm, NewAuthenticationForm
from django.contrib.auth import login, authenticate
from django.contrib import messages
from django.contrib.auth import logout


def home(request):
    """
    Vista utilizada para dirigir a la template home
    :param request:
    :return:
    """
    return render(request, 'user/home.html')


def user_register(request):
    """
    Se valida el metodo de la solicitud, si es post se instancia el formulario de creacion de usuarios propio de
    Django. Se valida si la informacion del formulario es correcta, si lo es se almacena la información del usuario en la db
    y se redigire a un intento de login automaticamente y se redirecciona al index.

    Si el metodo no es POST se carga el formulario de registro. Si ocurre algun error se le notifica al usuario.
    """
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registro Exitoso!.")
            return redirect("/")
        messages.error(request, "Algo paso :( , verifica tus datos.")
    form = NewUserForm()
    return render(request, "user/register.html", {"register_form": form})


def user_login(request):
    """
    Vista utilizada para renderizar un formulario de inicio de sesión, si el metodo de la request es POST se
    limpia y valida la informacion enviada por el formulario, y si esta es valida se inicia sesión, de lo contario
    se envia un error.
    :param request:
    :return:
    """
    if request.method == "POST":
        form = NewAuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                messages.error(request, "Usuario o contraseña incorrectos.")
        else:
            messages.error(request, "Usuario o contraseña incorrectos.")
    form = NewAuthenticationForm()
    return render(request=request, template_name="user/login.html", context={"login_form": form})


def user_logout(request):
    logout(request)
    messages.info(request, f"Adios, vuelva pronto!.")
    return redirect("login")
