from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.contrib.auth.models import User


class NewAuthenticationForm(AuthenticationForm):
    """
    Se sobre escriben los campos username y password para poder aplicarles los estilos css requeridos.
    """
    username = UsernameField(
        widget=forms.TextInput(attrs={"autofocus": True, 'class': 'form-control form-login', 'id': 'username',
                                      'aria-describedby': 'Usuario', 'placeholder': 'Usuario'}))
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(
            attrs={"autocomplete": "current-password", 'class': 'form-control form-login', 'type': 'password',
                   'id': 'password', 'aria-describedby': 'Contraseña', 'placeholder': '• • • • • • •'}),
    )


class NewUserForm(UserCreationForm):
    """
    Se crea una clase que hereda de las clases propias del modulo de autenticacion de django.
    Se definen los campos requeridos y opcionales. Se  modifican las labels y texto de ayuda ya que
    originalmente estan en ingles ademas de aplicar los estilos CSS que manejará cada elemento html renderizado por
    el widget elegido.
    """
    first_name = forms.CharField(max_length=50, required=True, label="Nombres", widget=forms.TextInput(
        attrs={'type': 'nombre', 'class': 'form-control form-login', 'id': 'nombre', 'aria-describedby': 'nombre',
               'placeholder': 'Nombre'}))
    last_name = forms.CharField(max_length=50, required=True, label="Apellidos", widget=forms.TextInput(
        attrs={'type': 'apellido', 'class': 'form-control form-login', 'id': 'apellido', 'aria-describedby': 'apellido',
               'placeholder': 'Apellido'}))
    username = forms.CharField(max_length=50, required=True, label="Usuario", widget=forms.TextInput(
        attrs={'type': 'usuario', 'class': 'form-control form-login', 'id': 'usuario', 'aria-describedby': 'usuario',
               'placeholder': 'Usuario'}))
    email = forms.EmailField(required=True, label="Correo Electronico", widget=forms.EmailInput(
        attrs={'type': 'correo', 'class': 'form-control form-login', 'id': 'correo', 'aria-describedby': 'correo',
               'placeholder': 'Correo electronico'}))
    """
    Se personaliza el campo de contraseña para poder mostrar las usgerencias en español.
    """
    password1 = forms.CharField(
        label="Contraseña",
        strip=False,
        widget=forms.PasswordInput(
            attrs={"autocomplete": "new-password", 'type': 'password', 'class': 'form-control form-login',
                   'id': 'password', 'aria-describedby': 'contraseña', 'placeholder': 'Contraseña'}),
        # help_text=password_validation.password_validators_help_text_html(),
        help_text="<ul><li>Su contraseña no puede ser demasiado similar a su otra información personal.</li><li>Su "
                  "contraseña debe contener al menos 8 caracteres.</li><li>Su contraseña no puede ser una contraseña "
                  "de uso común.</li><li>Su contraseña no puede ser completamente numérica.</li></ul> "
    )

    password2 = forms.CharField(
        label="Confirme su contreseña",
        widget=forms.PasswordInput(
            attrs={"autocomplete": "new-password", 'type': 'password', 'class': 'form-control form-login',
                   'id': 'password2', 'aria-describedby': 'confi-contraseña', 'placeholder': 'Contraseña'}),
        strip=False,
        help_text="Repita la contraseña previamente ingresada.",
    )
    city = forms.CharField(max_length=50, label="Ciudad", required=False, widget=forms.TextInput(
        attrs={'type': 'ciudad', 'class': 'form-control form-login', 'id': 'ciudad', 'aria-describedby': 'ciudad',
               'placeholder': 'Ciudad'}))
    adress = forms.CharField(max_length=50, label="Dirección", required=False, widget=forms.TextInput(
        attrs={'type': 'direccion', 'class': 'form-control form-login', 'id': 'direccion',
               'aria-describedby': 'direccion', 'placeholder': 'Dirección'}))
    phone = forms.CharField(max_length=50, label="Telefono", required=False, widget=forms.TextInput(
        attrs={'type': 'tel', 'class': 'form-control form-login', 'id': 'telefono', 'aria-describedby': 'telefono',
               'placeholder': 'Telefono'}))

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1", "password2", "city", "adress", "phone")

    def save(self, commit=True):
        """
        Metodo para guardar los usuarios en la db
        """
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
